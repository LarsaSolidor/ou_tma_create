from pathlib import Path
from shutil import copyfile
from jinja2 import Environment, FileSystemLoader
import git
import gitlab


class TMA:
    MODULE_NAMES = {
        'T276': 'Engineering Professions Practice and Skills 2',
        'T229': 'Mechanical Engineering: Heat and Flow',
        'T848': 'Managing Technological Innovation',
        'T849': 'Strategy for Technological Innovation',
        'T367': 'Solid Mechanics and Materials Failure',
        'T805': 'Manufacturing Materials Design',
        'T329': 'Mechanical Engineering B',
        'T460': 'MEng Individual Project Module',
        'T804': 'Finite Element Analysis',
        'T885': 'Team Engineering'
    }

    def __init__(self, assignment_number, module_code, module_name, no_questions, parent_dir):
        self.assignment_number = assignment_number
        self.module_code = module_code
        self.module_name = module_name
        self.no_questions = no_questions
        self.parent_dir = Path(parent_dir)

        self.tma_dir = self.parent_dir / f'TMA{self.assignment_number}'
        self.img_dir = self.tma_dir / 'Images'

        self.TMA_Q = ''
        self.generate_TMA_Q_doc()
        self.TMA_Q_session = ''
        self.generate_tex_studio_session()

    def create_folders(self):
        Path.mkdir(self.tma_dir)
        Path.mkdir(self.img_dir)

    def generate_TMA_Q_doc(self):
        file_loader = FileSystemLoader('./')

        env = Environment(loader=file_loader)

        template = env.get_template('TMA_Q.Rnw')

        self.TMA_Q = template.render(assignment_number=f'{{{self.assignment_number}}}',
                                     module_code=f'{{{self.module_code}}}',
                                     module_name=f'{{{self.module_name}}}',
                                     numb_questions=int(self.no_questions))

    def generate_tex_studio_session(self):
        file_loader = FileSystemLoader('./')

        env = Environment(loader=file_loader)

        template = env.get_template('TMA_Q.txss')

        self.TMA_Q_session = template.render(numb_questions=int(self.no_questions))

    def copy_files(self):
        copyfile('./AssignmentTemplate.Rnw', self.tma_dir / 'AssignmentTemplate.Rnw')
        # copyfile('./TMA_Q.Rnw', self.tma_dir / 'TMA_Q.Rnw')
        copyfile('./references.bib', self.tma_dir / 'references.bib')
        copyfile('./.gitignore', self.tma_dir / '.gitignore')

        with open(self.tma_dir / 'TMA_Q.Rnw', 'w') as file:
            file.write(self.TMA_Q)

        with open(self.tma_dir / 'TMA_Q.txss', 'w') as file:
            file.write(self.TMA_Q_session)

        copyfile('pdf_to_images_to_word.py', self.tma_dir / 'pdf_to_images_to_word.py')

        for q_num in range(1, int(self.no_questions) + 1):
            copyfile('./TMA_Q1.Rnw', self.tma_dir / f'TMA_Q{q_num}.Rnw')

    def commit_first_git_commit(self):
        r = git.Repo.init(self.tma_dir.as_posix())
        path_list = self.tma_dir.glob('*')
        for path in path_list:
            if not path.name == '.git':
                r.index.add([path.name])

        r.index.commit('Folder structure auto-generated')

    def create_repo_on_gitlab(self):
        try:
            gl = gitlab.Gitlab('https://gitlab.com', private_token='pEHtYuyJ3oo-b1XgXoXN')
            # group_id = gl.groups.search('open-university-lagb')[0].id
            project = gl.projects.create({'name': f'{self.module_code}-TMA{self.assignment_number}',
                                          'namespace_id': '3881817'})
            project.save()
            print('Done!')
        except gitlab.exceptions.GitlabCreateError:
            print('Repo already created on GitLab. Skipping.')

    def push_to_gitlab_repo(self):
        gitlab_project_name = f'{self.module_code}-tma{self.assignment_number}'.lower()

        r = git.Repo(self.tma_dir.as_posix())
        remote = r.create_remote(f'{self.module_code}-TMA{self.assignment_number}',
                                 url=f'https://gitlab.com/open-university-lagb/{gitlab_project_name}')
        remote.push(refspec='master:master')


if __name__ == '__main__':
    parent_dir = str(input(f'Parent directory: '))

    guessed_module_code = Path(parent_dir).name[:4]
    print(f'Guessed the module code from parent directory as: {guessed_module_code}')
    if 'y' in str(input('Accept this module code? [Y/n] ')):
        module_code = guessed_module_code
    else:
        module_code = input('Module Code: ')
    try:
        module_name = TMA.MODULE_NAMES[module_code]
        print(f'Found module name: {module_name}')
    except KeyError:
        print(f'Could not find module name in predefined list. Please enter manually.')
        module_name = input('Module Name: ')

    assignment_number = input('Assignment Number: ')
    no_questions = input('Number of Questions: ')
    create_gitlab = input('Create project on Gitlab and push? [y/n]')

    tma = TMA(assignment_number, module_code, module_name, no_questions, parent_dir)

    print('Creating folders...')
    tma.create_folders()
    print('Done!')
    print('Generating files...')
    tma.copy_files()
    print('Done!')
    print('Committing files to git...')
    tma.commit_first_git_commit()
    print('Done!')

    if 'y' in str(create_gitlab):
        print('Creating repo on GitLab...')
        tma.create_repo_on_gitlab()
        print('Pushing repo to GitLab...')
        tma.push_to_gitlab_repo()
        print('All finished.')
    else:
        print('All finished.')
