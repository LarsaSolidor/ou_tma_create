# Open University Tutor Marked Assignment Creator
Generates a folder with knitr files ready to start writing a TMA, then commits
those files using git, before optionally pushing to a new GitLab project under
open-university-lagb