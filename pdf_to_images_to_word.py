import fitz
import docx
from docx.shared import Cm
from pathlib import Path

pdffile = "TMA_Q.pdf"
doc = fitz.open(pdffile)


if Path('TMA_Q.docx').exists():
	Path('TMA_Q.docx').unlink()
# Path('TMA_Q.docx').touch()
word_doc = docx.Document()

for sheet in range(0, doc.pageCount):
	page = doc.loadPage(sheet) #number of page
	pix = page.getPixmap(matrix=fitz.Matrix(2.0, 2.0))
	output = f"Page_{sheet:02}.png"
	pix.writePNG(f"./Assignment as Images/{output}")
	# image = pix.getPNGData()
	# word_doc.add_picture(image)

for image in Path("./Assignment As Images").glob('*.png'):
	print(image.absolute())
	word_doc.add_picture(str(image.absolute()), width = Cm(21), height= Cm(29.7))

sections = word_doc.sections
for section in sections:
	section.top_margin = Cm(0)
	section.bottom_margin = Cm(0)
	section.left_margin = Cm(0)
	section.right_margin = Cm(0)
	section.page_height = Cm(29.7)
	section.page_width = Cm(21)


word_doc.save('TMA_Q.docx')

